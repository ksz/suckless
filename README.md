### [suckless.org](https://suckless.org/)

# dwm

[suckless dynamic window manager](https://dwm.suckless.org/)

dwm-6.3

```
patch -p1 < patches/dwm-center-6.2.diff
patch -p1 < patches/dwm-hide_vacant_tags-6.3.diff
patch -p1 < patches/dwm-noborderfloatingfix-6.2.diff
patch -p1 < patches/dwm-pertag-6.2.diff
patch -p1 < patches/dwm-scratchpad-6.2.diff
patch -p1 < patches/dwm-status2d-6.3.diff
patch -p1 < patches/dwm-statuscmd-nosignal-status2d-6.3-mod.diff
patch -p1 < patches/dwm-vanitygaps-6.3-mod.diff
```

# st

[suckless simple terminal](https://st.suckless.org/)

st-0.8.5

```
patch -p1 < patches/st-alpha-20220206-0.8.5.diff
patch -p1 < patches/st-gruvbox-dark-0.8.5.diff
patch -p1 < patches/st-scrollback-0.8.5.diff
patch -p1 < patches/st-scrollback-reflow-0.8.5.diff
patch -p1 < patches/st-scrollback-mouse-20220127-2c5edf2.diff
patch -p1 < patches/st-scrollback-mouse-altscreen-20220127-2c5edf2.diff
```

# dmenu

[suckless dynamic menu](https://tools.suckless.org/dmenu/)

dmenu-5.2

```
patch -p1 < patches/dmenu-mousesupport-5.2.diff
patch -p1 < patches/dmenu-mousesupport-motion-5.2.diff
patch -p1 < patches/dmenu-highlight-20201211-fcdc159.diff
patch -p1 < patches/dmenu-numbers-20220512-28fb3e2.diff
```

# slock

[suckless screen locker](https://tools.suckless.org/slock/)

slock-1.5

```
patch -p1 < patches/slock-alternate-colors-20220921-35633d4.diff
patch -p1 < patches/slock-message-20191002-b46028b.diff
```

# slstatus

[suckless status monitor](https://tools.suckless.org/slstatus/)

slstatus-1.0
