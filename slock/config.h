/* user and group to drop privileges to */
static const char *user  = "root";
static const char *group = "root";

static const char *colorname[NUMCOLS] = {
	[INIT] =      "black",     /* after initialization */
	[INPUT] =     "#005577",   /* during input */
	[INPUT_ALT] = "#227799",   /* during input, second color */
	[FAILED] =    "#CC3333",   /* wrong password */
};

/* treat a cleared input like a wrong password (color) */
static const int failonclear = 1;

/* default message */
static const char * message = "suckless.org - software that sucks less";

/* text color */
static const char * text_color = "#ffffff";

/* text size (must be a valid size) */
static const char * font_name = "6x13";
